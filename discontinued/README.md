# Miage - ias 2023

Page pour le cours IAS des Miage en apprentissage, 2023

Pour télécharger ce repo (repository, dossier distant), faites:
`git clone https://gitlab.inria.fr/flandes/miage-ias.git`

Pour le mettre à jour (car des corrigés ou autres fichiers seront progressivement ajoutés/modifiés)
`git pull`
depuis le dossier où vous avez cloné. (Ainsi, vous ne téléchargez que les différences, c'est plus écolo).

Licence du poly de **cours/TD**: [CC-BY-SA] https://fr.wikipedia.org/wiki/Licence_Creative_Commons

Ce dossier contient/contiendra tous les supports liés au cours, dont:
- le pdf du CM lui même.
- le pdf des TD/TP
- les Notebooks qui servent de support au poly
- les Notebooks qui servent d'énoncés de TP.

Les jeux de données utiles pour le cours sont plutôt situées ici (car mutualisées entre plusieurs cours):
https://gitlab.inria.fr/flandes/data-for-teaching

Service jupyter-cloud, si vous n'avez pas jupyter à portée de main:
https://jupyterhub.ijclab.in2p3.fr/


Enregistrement vidéos:
https://www.youtube.com/watch?v=GdKSlTbytnc&list=PL4gWpBshvobPNdXmERJ6TIPKveLBYdgE3&index=1

## Contenu des CM (prévisionnel)

attention, un module est une unité logique, mais ne correspondra pas forcément pile à une seule séance

- module1-apercuML
    + bref aperçu du ML, du vocabulaire

- module2-regressionLineaire-DescenteDeGradient
    + Ce module prendre p-e plus que une séance
    + Regression Linéaire
    + Descente de Gradient
    + démo de la descente de gradient en 1D et en 2D

- module3-Perceptron
    + Ce module prendre p-e plus que une séance
    + discussion complète du perceptron
    + on mentionne à la fin le SVM
    + classification multi-classe: on évoque sans rentrer dans le détail
    + démo de cours-Perceptron.ipynb
    + TODO: montrer aussi le perceptron sur MNIST, et aussi le cas multi-classe

- module4-overfitting
    + overfitting
    + les métriques

- module5-encodages+featureMaps+pre-procs+PCA
    + Ce module prendre p-e plus que une séance
    + encodages
    + feature maps
    + PCA
    + pre-procs: feature maps, standardization, PCA

- module6-autres modèles + contenu à discuter !
    + à voir - on fera le point d'ici là.


## Contenu des TDs/TPS (prévisionnel)

Attention, chaque dossier correspond à une unité logique, mais ne correspondra pas forcément pile à une séance (parfois moins, parfois plus)

- TD1-DescenteDeGradient-J(theta)
- TD2-RegressionLineaire
- TD3-equation de droite
    + Ce TD devrait prendre MOINS que une séance
- TD4-Perceptron-papier-crayon
    + Ce TD devrait prendre MOINS que une séance
- TD5-Perceptron-code
    + Ce TD prendra p-e PLUS de temps que une séance
- TD6-PCA+SVM-optimisation1hyper-param
    + Ce TD prendra p-e PLUS de temps que une séance
- TD7-prise en main d’un problème
    + Ce TD prendra p-e PLUS de temps que une séance
- TD8-séance de TP-projet
    + séance de projet: pas de contenu en propre prévu à l'avance.
- TD9-analyseResultatExp
    + Ce TD peut être sauté si le temps manque

On peut aussi envisager un TD de révisions pour l'examen comme dernier TD/TP.
